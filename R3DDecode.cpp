#include "mex.h"
#include <R3DSDK.h>

#include <algorithm>
#include <vector>
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>

#define IS_SCALAR(P)  (mxGetNumberOfElements(P) == 1)

#define FNAME   (prhs[0])
#define COUNT   (prhs[1])
#define OFFSET  (prhs[2])

#define FRAMES  (plhs[0])
#define REM     (plhs[1])

using namespace R3DSDK;

void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[])
{
  if ( nrhs != 3 )
  {
    mexPrintf("Incorrect number of parameters.\n");
    mexPrintf("Usage: [<images>, <remaining frames>] = R3DDecode(<input file name>, <number of frames>, <offset>)\n");
    return;
  }
  if ( nlhs != 2 )
  {
    mexPrintf("Incorrect number of return variables.\n");
    mexPrintf("Usage: [<images>, <remaining frames>] = R3DDecode(<input file name>, <number of frames>, <offset>)\n");
    return;
  }
  if ( !mxIsChar(FNAME) )
  {
    mexPrintf("Incorrect type for 1st parameter.\n");
    mexPrintf("Usage: [<images>, <remaining frames>] = R3DDecode(<input file name>, <number of frames>, <offset>)\n");
    mexPrintf("Parameter: input file name - string\n");
    return;
  }
  if ( !mxIsDouble(COUNT) || !IS_SCALAR(COUNT) )
  {
    mexPrintf("Incorrect type for 2nd parameter.\n");
    mexPrintf("Usage: [<images>, <remaining frames>] = R3DDecode(<input file name>, <number of frames>, <offset>)\n");
    mexPrintf("Parameter: number of frames - scalar numeric\n");
    return;
  }
  if ( !mxIsDouble(OFFSET) || !IS_SCALAR(OFFSET) )
  {
    mexPrintf("Incorrect type for 3rd parameter.\n");
    mexPrintf("Usage: [<images>, <remaining frames>] = R3DDecode(<input file name>, <number of frames>, <offset>)\n");
    mexPrintf("Parameter: offset - scalar numeric\n");
    return;
  }

  char * const fname = mxArrayToString(FNAME);
  const size_t count = (size_t)(*mxGetPr(COUNT));
  const size_t offset = (size_t)(*mxGetPr(OFFSET));

  InitializeStatus init_status = InitializeSdk(".", OPTION_RED_NONE);
  if( init_status != ISInitializeOK )
  {
    mexPrintf("Unable to load R3D Dynamic lib %d\n", init_status);
    FinalizeSdk();
    free(fname);
    return;
  }

  Clip *clip = new Clip(fname);
  if( clip->Status() != LSClipLoaded )
  {
    mexPrintf("Failed to load clip %s. Status: %d\n", fname, clip->Status());
    delete clip;
    FinalizeSdk();
    free(fname);
    return;
  }
  free(fname);

  size_t frames = clip->VideoFrameCount();
  size_t last = std::min(frames, count + offset);
  size_t width = clip->Width();
  size_t height = clip->Height();
  size_t memNeeded = width * height * 3U * 2U;

  VideoDecodeJob job;
  job.BytesPerRow = width * 2U;
  job.OutputBufferSize = memNeeded;
  job.Mode = DECODE_FULL_RES_PREMIUM;
  job.PixelType = PixelType_16Bit_RGB_Planar;

  mxArray *imgs = mxCreateCellMatrix(last - offset, 1);
  const mwSize dims[] = {width, height, 3U};
  for(size_t frame = offset; frame < last; frame++)
  {
    mxArray *img = mxCreateNumericArray(3U, dims, mxUINT16_CLASS, mxREAL);
    job.OutputBuffer = mxGetData(img);
    if (clip->DecodeVideoFrame(frame, job) != DSDecodeOK)
    {
      mexPrintf("Decode failed\n");
      delete clip;
      FinalizeSdk();
      return;
    }
    mxSetCell(imgs, frame-offset, img);
  }

  FRAMES = imgs;
  REM = mxCreateDoubleScalar(frames - last);

  delete clip;
  FinalizeSdk();
  return;
}
