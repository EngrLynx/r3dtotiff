function R3D2Tif(r3dfile, tifdir, count, batsize)
  ischar(r3dfile) || ...
    error('R3D filename must be a string');
  ischar(tifdir) || ...
    error('Tiff output directory must be a string');
  isnumeric(count) && isscalar(count) || ...
    error('Count must be a numeric scalar');
  isnumeric(batsize) && isscalar(batsize) || ...
    error('Batch size must be a numeric scalar');
  mkdir(tifdir);
  fname_splt = strsplit(r3dfile, '.');
  pre_splt = fname_splt(1 : size(fname_splt, 2) - 1);
  pre = strjoin(pre_splt, '.');
  offset = 0;
  while offset < count
    batch_cnt = min(batsize, count - offset);
    [images, rem] = R3DDecode(r3dfile, batch_cnt, offset);
    for ndx = 1 : size(images, 1)
      fname = strcat(tifdir, '/', pre, '.', ...
        int2str(offset+ndx), '.', 'tif');
      imwrite(images{ndx}, fname, 'tiff');
    end
    if rem == 0
      break;
    end
    offset = offset + batch_cnt;
  end
end